--Crearea bazei de date:
CREATE SCHEMA `warehouse` ;
--Crearea tabelelor PRODUCT, CLIENT, ORDER.
--PRODUCT
CREATE TABLE `warehouse`.`product` (
  `productId` DOUBLE NOT NULL,
  `productName` VARCHAR(80),
  `productStock` FLOAT,
  `measureUnit` VARCHAR(80),
  `productPrice` FLOAT,
  PRIMARY KEY (`productId`));
  -- CLIENT:
  CREATE TABLE `warehouse`.`client` (
  `clientId` DOUBLE NOT NULL,
  `firstName` VARCHAR(80),
  `lastName` VARCHAR(80),
  `address` VARCHAR(80),
  `phone` VARCHAR(80),
  `emailAddress` VARCHAR(80),
  PRIMARY KEY (`clientId`));
  --ORDER:
 CREATE TABLE `warehouse`.`order` (
  `orderId` DOUBLE NOT NULL,
  `clientId` DOUBLE NOT NULL,
  `productId` DOUBLE NOT NULL,
  `orderStatus` VARCHAR(80),
  `orderDate` VARCHAR(80),
  PRIMARY KEY (`orderId`));
  
  --PRODUCT_SET (tabel de legatura)
  CREATE TABLE `warehouse`.`product_set` (
  `OrderId` DOUBLE NOT NULL,
  `ProductId` DOUBLE NOT NULL,
  PRIMARY KEY (`OrderId`, `ProductId`));
 
--Crearea unui index in coloana-copil:
ALTER TABLE `warehouse`.`client` 
ADD INDEX `product_id` (`productId` ASC) VISIBLE;
;
--Crearea cheii straine:
ALTER TABLE `warehouse`.`client` 
ADD CONSTRAINT `productId_client_fk`
  FOREIGN KEY (`productId`)
  REFERENCES `warehouse`.`product` (`productId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
--Crearea unui index in coloana-copil:
ALTER TABLE `warehouse`.`order` 
ADD INDEX `product_id` (`productId` ASC) VISIBLE;
;
--Crearea cheii straine:
ALTER TABLE `warehouse`.`order` 
ADD CONSTRAINT `productId_order_fk`
  FOREIGN KEY (`productId`)
  REFERENCES `warehouse`.`product` (`productId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;
  
 --Crearea unui index in coloana-copil:
  ALTER TABLE `warehouse`.`client` 
ADD INDEX `order_id` (`orderId` ASC) VISIBLE;
;
--Crearea cheii straine:
ALTER TABLE `warehouse`.`client` 
ADD CONSTRAINT `orderId_fk`
  FOREIGN KEY (`orderId`)
  REFERENCES `warehouse`.`order` (`orderId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

 --Crearea unui index in coloana-copil:
ALTER TABLE `warehouse`.`order` 
ADD INDEX `clientId` (`clientId` ASC) VISIBLE;
;
--Crearea cheii straine:
ALTER TABLE `warehouse`.`order` 
ADD CONSTRAINT `clientId_fk`
  FOREIGN KEY (`clientId`)
  REFERENCES `warehouse`.`client` (`clientId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

 --Crearea unui index in coloana-copil:
ALTER TABLE `warehouse`.`product_set` 
ADD INDEX `OrderId` (`OrderId` ASC) INVISIBLE,
ADD INDEX `ProductId` (`ProductId` ASC) VISIBLE;
;

--Crearea cheii straine:
ALTER TABLE `warehouse`.`product_set` 
ADD CONSTRAINT `orderId_fk_sp`
  FOREIGN KEY (`OrderId`)
  REFERENCES `warehouse`.`order` (`orderId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
ADD CONSTRAINT `productId_fk_sp`
  FOREIGN KEY (`ProductId`)
  REFERENCES `warehouse`.`product` (`productId`)
  ON DELETE CASCADE
  ON UPDATE CASCADE;

--Inseare randuri in tabelul Order:
INSERT INTO `warehouse`.`order` (`orderId`, `clientId`, `orderStatus`, `orderDate`) VALUES ('2', '3', 'In curs de procesare', '20.04.2020');
INSERT INTO `warehouse`.`order` (`orderId`, `clientId`, `orderStatus`, `orderDate`) VALUES ('3', '2', 'Anulata', '10.10.2019');
INSERT INTO `warehouse`.`order` (`orderId`, `clientId`, `orderStatus`, `orderDate`) VALUES ('4', '1', 'Livrata', '03.05.2019');

--Inseare randuri in tabelul Product:
INSERT INTO `warehouse`.`product` (`productId`, `productName`, `productQuantity`, `measureUnit`, `productPrice`) VALUES ('2', 'Brocoli', '3', 'kg', '12.9');

----Inseare randuri in tabelul Product_Set:
INSERT INTO `warehouse`.`product_set` (`OrderId`, `ProductId`) VALUES ('1', '2');
INSERT INTO `warehouse`.`product_set` (`OrderId`, `ProductId`) VALUES ('1', '3');
INSERT INTO `warehouse`.`product_set` (`OrderId`, `ProductId`) VALUES ('2', '1');
INSERT INTO `warehouse`.`product_set` (`OrderId`, `ProductId`) VALUES ('2', '2');

--Inseare randuri in tabelul Client:
INSERT INTO `warehouse`.`client` (`clientId`, `firstName`, `lastName`, `address`, `phone`, `emailAddress`) VALUES ('1', 'Lungu', 'Simona', 'Strada Inau nr 5', '0757362634', 'lungu.simona@yahoo.com');


