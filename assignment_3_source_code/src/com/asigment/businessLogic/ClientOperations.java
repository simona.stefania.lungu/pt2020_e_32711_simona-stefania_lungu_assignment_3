package com.asigment.businessLogic;

import com.asigment.model.Client;
import com.mysql.cj.x.protobuf.MysqlxPrepare;
import com.sun.org.apache.xpath.internal.operations.Or;
import org.omg.CORBA.CODESET_INCOMPATIBLE;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientOperations {
    // Operatia de Insert:
    public void addClient(Connection connection, Client client) throws SQLException {
        String sqlQuerry = "INSERT INTO client (clientId, firstName, lastName, address, phone, emailAddress) VALUES (?,?,?,?,?,?)";
        PreparedStatement statement = connection.prepareStatement(sqlQuerry);
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        statement.setLong(1, client.getClientId());
        statement.setString(2, client.getFirstName());
        statement.setString(3, client.getLastName());
        statement.setString(4, client.getAddress());
        statement.setString(5, client.getPhone());
        statement.setString(6, client.getEmailAddress());

        statement.executeUpdate();
        System.out.println("Operatia de insert s-a efectuat cu succes!");
    }

    // Operatia de Read dupa clientId:
    public Client readClient(Connection con, long Id) throws SQLException {
        String sqlQuery = "SELECT * FROM client WHERE clientId = ?";
        PreparedStatement preparedStatement = con.prepareStatement(sqlQuery);
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        preparedStatement.setLong(1, Id);
        System.out.println("Looking for client..");
        ResultSet resultSet = preparedStatement.executeQuery(); // se executa SQL query si sunt returnate rezultatele din baza de date
        Client clientOne = new Client();

        while (resultSet.next()) {
            // sunt aduse din baza de date rezultatele interogarii.
            long clientId = resultSet.getLong("clientId");
            String clientFName = resultSet.getString("firstName");
            String clientLName = resultSet.getString("lastName");
            String address = resultSet.getString("address");
            String phone = resultSet.getString("phone");
            String email = resultSet.getString("emailAddress");
            // popularea campurilor pentru Client
            clientOne.setClientId(clientId);
            clientOne.setFirstName(clientFName);
            clientOne.setLastName(clientLName);
            clientOne.setAddress(address);
            clientOne.setPhone(phone);
            clientOne.setEmailAddress(email);
        }
        return clientOne;
    }

    // Operatia de Read dupa firstName si lastName:
        public Client readClientbyName(Connection con, String firsName, String lastName) throws SQLException{
        Client client1 = new Client();
        String sqlQuery = "SELECT * FROM client WHERE firstName = ? AND lastName = ?";
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        PreparedStatement preparedStatement = con.prepareStatement(sqlQuery);
        preparedStatement.setString(1,firsName);
        preparedStatement.setString(2,lastName);
        ResultSet resultSet = preparedStatement.executeQuery(); // se executa SQL query si sunt returnate rezultatele din baza de date
            while(resultSet.next()){
                // sunt aduse din baza de date rezultatele interogarii.
                long clientId = resultSet.getLong("clientId");
                String clientFName = resultSet.getString("firstName");
                String clientLName = resultSet.getString("lastName");
                String address = resultSet.getString("address");
                String phone = resultSet.getString("phone");
                String email = resultSet.getString("emailAddress");

                // popularea campurilor pentru Client
                client1.setClientId(clientId);
                client1.setFirstName(clientFName);
                client1.setLastName(clientLName);
                client1.setAddress(address);
                client1.setPhone(phone);
                client1.setEmailAddress(email);
            }
        return client1;
        }
    // Operatia de Read ALL:
    public Client[] readAll(Connection connection) throws SQLException{
        int i = 0;
        Client[] clients = new Client[100];
        String sqlQuery  = " SELECT * FROM client";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        ResultSet resultSet = preparedStatement.executeQuery();  // se executa SQL query si sunt returnate rezultatele din baza de date;
            while(resultSet.next()){
                Client clientReadAll = new Client();
                // sunt aduse din baza de date rezultatele interogarii.
                long clientId = resultSet.getLong("clientId");
                String clientFName = resultSet.getString("firstName");
                String clientLName = resultSet.getString("lastName");
                String address = resultSet.getString("address");
                String phone = resultSet.getString("phone");
                String email = resultSet.getString("emailAddress");

                // popularea campurilor pentru Client:
                clientReadAll.setClientId(clientId);
                clientReadAll.setFirstName(clientFName);
                clientReadAll.setLastName(clientLName);
                clientReadAll.setAddress(address);
                clientReadAll.setPhone(phone);
                clientReadAll.setEmailAddress(email);
                clients[i] = clientReadAll;
                i++;
            }
            return clients;
    }

    // Operatia de Update:
    public void updateClient(Connection con, Client client) throws SQLException {
        String sqlQuerry = "UPDATE client SET firstName = ?, lastName = ?, address = ?, phone = ?, emailAddress = ? WHERE clientId = ?";
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        PreparedStatement preparedStatement = con.prepareStatement(sqlQuerry);
        preparedStatement.setString(1, client.getFirstName());
        preparedStatement.setString(2, client.getLastName());
        preparedStatement.setString(3,client.getAddress());
        preparedStatement.setString(4, client.getPhone());
        preparedStatement.setString(5, client.getEmailAddress());
        preparedStatement.setLong(6, client.getClientId());

        preparedStatement.executeUpdate(); // se executa SQL query
        System.out.println("Operatia de update s-a efectuat cu succes!");
    }

    // Operatia de Delete:
    public void deleteClient(Connection con, long clientId) throws SQLException {
        // Stergerea clientului din tabelul Client dupa clientId:
        String sqlQuery = "DELETE FROM client WHERE clientId = ?";
        PreparedStatement preparedStatement = con.prepareStatement(sqlQuery);
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        preparedStatement.setLong(1, clientId);
        preparedStatement.executeUpdate();  // se executa SQL query
        System.out.println("Clientul cu clientId = "+clientId+" a fost sters din tabelul Client.");
        OrderOperations orderOperations = new OrderOperations();
        orderOperations.deleteClient(con,clientId);

    }

    public long maxClientId(Connection connection) throws  SQLException{
        long maxClientIdNoData = 0;
        String sqlQuery = "SELECT MAX(clientId) AS clientId FROM `client`";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        ResultSet maxClientId = preparedStatement.executeQuery();  // se executa SQL query si sunt aduse rezultatele din baza de date
        if(maxClientId.next()) {
            maxClientIdNoData=  maxClientId.getLong("clientId");
        }

        return maxClientIdNoData;
    }

}

