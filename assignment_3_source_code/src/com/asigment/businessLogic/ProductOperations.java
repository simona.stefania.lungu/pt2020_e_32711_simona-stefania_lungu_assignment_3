package com.asigment.businessLogic;

import com.asigment.model.Product;
import com.mysql.cj.xdevapi.Client;

import java.lang.invoke.SwitchPoint;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductOperations {

    // Operatia de Insert:
    public void addProduct(Connection connection, Product newProduct) throws SQLException{
        String sqlQuery = "INSERT INTO product (productId, productName, productStock, measureUnit, productPrice) VALUES (?, ?,?,?,?)";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        preparedStatement.setLong(1,newProduct.getProductId());
        preparedStatement.setString(2,newProduct.getProductName());
        preparedStatement.setFloat(3, newProduct.getproductStock());
        preparedStatement.setString(4,newProduct.getMeasureUnit());
        preparedStatement.setFloat(5,newProduct.getProductPrice());
        preparedStatement.executeUpdate(); // se executa SQL query
    }

    // Operatia de Read by productId:
    public Product readProductById(Connection con, long Id ) throws SQLException {
        String sqlQuery = "SELECT * FROM product WHERE productId = ?";
        PreparedStatement preparedStatement = con.prepareStatement(sqlQuery);
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        preparedStatement.setLong(1, Id);
        System.out.println("Looking for product..");
        ResultSet resultSet = preparedStatement.executeQuery(); // se executa SQL query si sunt aduse rezultatele din baza de date;
        Product product = new Product();
        while (resultSet.next()) {
            // sunt aduse din baza de date rezultatele interogarii:
            long productId = resultSet.getLong("productId");
            String productName = resultSet.getString("productName");
            float productStock = resultSet.getFloat("productStock");
            String measureUnit = resultSet.getString("measureUnit");
            float productPrice = resultSet.getFloat("productPrice");

            // popularea campurilor pentru Product:
            product.setProductId(productId);
            product.setProductName(productName);
            product.setproductStock(productStock);
            product.setMeasureUnit(measureUnit);
            product.setProductPrice(productPrice);
        }
        return product;
    }
    // Operatia de Read by productName:
    public Product readProductByName(Connection con, String name) throws SQLException{
        Product product = new Product();
        String sqlQuerry = "SELECT * FROM product WHERE productName = ?";
        PreparedStatement preparedStatement = con.prepareStatement(sqlQuerry);
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        preparedStatement.setString(1,name);
        ResultSet resultSet = preparedStatement.executeQuery();  // se executa SQL query si sunt aduse rezultatele din baza de date;
        while(resultSet.next()){
            // sunt aduse din baza de date rezultatele interogarii:
            long productId = resultSet.getLong("productId");
            String productName = resultSet.getString("productName");
            float productStock = resultSet.getFloat("productStock");
            String measureUnit = resultSet.getString("measureUnit");
            float productPrice = resultSet.getFloat("productPrice");

        // popularea campurilor pentru Product:
        product.setProductId(productId);
        product.setProductName(productName);
        product.setproductStock(productStock);
        product.setMeasureUnit(measureUnit);
        product.setProductPrice(productPrice);
        }
        return product;
    }

    // Operatia de Read ALL:
        public Product[] readAll(Connection connection) throws SQLException{
        Product[] products =  new Product[100];
        int i = 0;
        String sqlQuery = "SELECT * FROM product";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        ResultSet resultSet = preparedStatement.executeQuery(); // se executa SQL query

            while(resultSet.next()){
                Product productReadAll = new Product();
                // sunt aduse din baza de date rezultatele interogarii:
                long productId = resultSet.getLong("productId");
                String productName = resultSet.getString("productName");
                float productStock = resultSet.getFloat("productStock");
                String measureUnit = resultSet.getString("measureUnit");
                float productPrice = resultSet.getFloat("productPrice");

                // popularea campurilor pentru Product:
                productReadAll.setProductId(productId);
                productReadAll.setProductName(productName);
                productReadAll.setproductStock(productStock);
                productReadAll.setMeasureUnit(measureUnit);
                productReadAll.setProductPrice(productPrice);
                products[i] = productReadAll;
                i++;
            }
            return products;
        }


    // Operatia de Update:
    public void productUpdate(Connection connection, Product product) throws SQLException{
        String sqlQuery = " UPDATE product SET productName = ?, productStock = ?, measureUnit = ?, productPrice = ? WHERE productId = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        preparedStatement.setString(1,product.getProductName());
        preparedStatement.setFloat(2,product.getproductStock());
        preparedStatement.setString(3,product.getMeasureUnit());
        preparedStatement.setFloat(4,product.getProductPrice());
        preparedStatement.setLong(5,product.getProductId());
        preparedStatement.executeUpdate(); // se executa SQL query
    }

    // Operatia de Delete:
    public void deleteProduct(Connection connection, long productId) throws  SQLException{
        ProductsSetOperations productsSetOperations = new ProductsSetOperations();
        productsSetOperations.deleteProductSetByProductId(connection,productId);
        String sqlQuerry ="DELETE FROM `product` WHERE ProductId = ?";
        PreparedStatement preparedStatement  = connection.prepareStatement(sqlQuerry);
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        preparedStatement.setLong(1, productId);
        preparedStatement.executeUpdate(); // se executa SQL query
    }

    // Returnarea celui mai mare clientId din baza de date: ( Este necesar sa stiu daca baza de date era deja populata inainte de a primi noi inregistrari preluate din fisier).
    public long maxProductId(Connection connection) throws  SQLException{
        long maxProductIdNoData = 0;
            String sqlQuery = "SELECT MAX(productId) AS productId FROM `product`";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            ResultSet maxProductId = preparedStatement.executeQuery();  // se executa SQL query si sunt aduse rezultatele din baza de date
            if(maxProductId.next()) {
                 maxProductIdNoData=  maxProductId.getLong("productId");
            }

        return maxProductIdNoData;
    }

}
