package com.asigment.businessLogic;

import com.asigment.model.Order;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class OrderOperations {
    // Operatia de Insert:
    public void addOrder (Connection con, Order newOrder) throws SQLException {
        String sqlQuery = "INSERT INTO `order` (orderId, clientId, orderStatus, orderDate) VALUES (?,?,?,?)";
        PreparedStatement preparedStatement = con.prepareStatement(sqlQuery);
        // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        preparedStatement.setLong(1,newOrder.getOrderId());
        preparedStatement.setLong(2,newOrder.getClientId());
        preparedStatement.setString(3, newOrder.getOrderStatus());
        preparedStatement.setString(4,newOrder.getOrderDate());

        preparedStatement.executeUpdate(); // se executa SQL query
        System.out.println("Operatia de insert s-a efectuat cu succes!");
    }
    // Operatia de Read:
     public Order readOrder(Connection con, long Id) throws SQLException {
         String sqlQuerry = "SELECT * FROM `order` WHERE orderId = ?";
         PreparedStatement preparedStatement = con.prepareStatement(sqlQuerry);
         // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
         preparedStatement.setLong(1, Id);
         ResultSet resultSet = preparedStatement.executeQuery();
         System.out.println("Looking for order..");
         Order order = new Order();

         while (resultSet.next()) {
             // sunt aduse din baza de date rezultatele interogarii.
             long orderId = resultSet.getLong("orderId");
             long clientId = resultSet.getLong("clientId");
             String orderStatus = resultSet.getString("orderStatus");
             String orderDate = resultSet.getString("orderDate");
             // popularea campurilor pentru Product
             order.setOrderId(orderId);
             order.setClientId(clientId);
             order.setOrderStatus(orderStatus);
             order.setOrderDate(orderDate);
         }
         return order;
     }
     //Operatia de Read ALL:
    public Order[] readAll(Connection connection) throws SQLException{
        Order[] orders = new Order[100];
        int i = 0;
        String sqlQuery = "SELECT * FROM `order`";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        ResultSet resultSet = preparedStatement.executeQuery();

        while(resultSet.next()){
            Order readAllOrder = new Order();
            // sunt aduse din baza de date rezultatele interogarii:
            long orderId = resultSet.getLong("orderId");
            long clientId = resultSet.getLong("clientId");
            String orderStatus = resultSet.getString("orderStatus");
            String orderDate = resultSet.getString("orderDate");

            // popularea campurilor pentru Product:
            readAllOrder.setOrderId(orderId);
            readAllOrder.setClientId(clientId);
            readAllOrder.setOrderStatus(orderStatus);
            readAllOrder.setOrderDate(orderDate);
            orders[i] = readAllOrder;
            i++;
        }
        return orders;
    }

    // Operatia de Update:
     public void updateByOrderId(Connection con, Order order) throws SQLException{
        String sqlQuery = "UPDATE `order` SET orderStatus = ?, orderDate = ? WHERE orderId = ?";
        PreparedStatement preparedStatement = con.prepareStatement(sqlQuery);
         // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
        preparedStatement.setString(1,order.getOrderStatus());
        preparedStatement.setString(2,order.getOrderDate());
        preparedStatement.setLong(3,order.getOrderId());
        preparedStatement.executeUpdate(); // se executa SQL query
         System.out.println("Operatia de Update s-a finalizat cu succes");
     }

    // Operatia de Delete:
        // Stergere dupa clientId: daca se elimina clientul din baza de date, automat se vor elimina si comenzile sale.

         public void deleteClient(Connection connection, long clientId) throws SQLException{

        // Cautarea comenzilor plasate de catre clientul cu acel clientId sters din baza de date:
             ProductsSetOperations productsSetOperations = new ProductsSetOperations();
             String sqlQueryOne = "SELECT * FROM `order` WHERE clientId = ? ";
             PreparedStatement preparedStatementOne = connection.prepareStatement(sqlQueryOne);
             // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
             preparedStatementOne.setLong(1,clientId);
             ResultSet resultSet = preparedStatementOne.executeQuery(); // se executa SQL query si sunt aduse rezultatele din baza de date;
             System.out.println("Looking for order..");
                 while (resultSet.next()) {
                     // Randurile corespunzatoare sunt aduse din baza de date:
                     long orderId = resultSet.getLong("orderId");
                     long clientId1 = resultSet.getLong("clientId");
                     String orderStatus = resultSet.getString("orderStatus");
                     String orderDate = resultSet.getString("orderDate");
                     // Stergerea comenzilor din tabelul de legatura Product_set dupa orderId:
                     productsSetOperations.deleteProductSetByOrderId(connection, orderId);
                 }
                    // Stergerea comenzilor dupa clientId:
                     String sqlQuery = "DELETE FROM `order` WHERE clientId = ?";
                     PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
                     preparedStatement.setLong(1, clientId);
                     preparedStatement.executeUpdate(); // se executa SQL query
                    System.out.println("Comenzile clientului cu clientId = "+clientId +" au fost eliminate.");
                 }

        // Delete Order:
             public void deleteOrder(Connection connection, long Id) throws SQLException {
            // Stergerea comenzilor din tabelul Order dupa orderId tabelul intermediar Product_set:
            ProductsSetOperations productsSetOperations = new ProductsSetOperations();
            productsSetOperations.deleteProductSetByOrderId(connection, Id);
            // Stergerea comenzilor din tabelul Order dupa orderId tabelul Order:
            String sqlQuery = "DELETE FROM `order` WHERE orderId = ?";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
                 // In locul semnelor de intrebare se pun urmatoarele valori preluate din obiectul care apartine clasei Client:
            preparedStatement.setLong(1,Id);
            preparedStatement.executeUpdate(); // se executa SQL query
            System.out.println("Operatia de stergere s-a efectuat cu succes!");
         }

        // Returnarea celui mai mare orderId din baza de date: ( Este necesar sa stiu daca baza de date era deja populata inainte de a primi noi inregistrari preluate din fisier).
        public long maxOrderId(Connection connection) throws  SQLException{
            long maxOrderIdNoData = 0;
            String sqlQuery = "SELECT MAX(orderId) FROM warehouse.order";
            PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
            ResultSet maxOrderId = preparedStatement.executeQuery();  // se executa SQL query si sunt aduse rezultatele din baza de date;
            if(maxOrderId.next()) {
                maxOrderIdNoData=  maxOrderId.getLong("orderId");
            }
            return maxOrderIdNoData;
        }
}
