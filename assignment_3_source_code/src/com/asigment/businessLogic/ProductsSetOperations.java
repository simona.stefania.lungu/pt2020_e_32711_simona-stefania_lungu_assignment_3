package com.asigment.businessLogic;
import com.asigment.model.Client;
import com.asigment.model.ProductsSet;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class ProductsSetOperations {
    public void addSetProducts(Connection con, ProductsSet newSetProducts ) throws SQLException{
        String sqlQuerry = "INSERT INTO product_set (OrderId,ProductId,ProductQuantity) VALUES (?,?,?)";
        PreparedStatement preparedStatement = con.prepareStatement(sqlQuerry);
        preparedStatement.setLong(1, newSetProducts.getOrderId());
        preparedStatement.setLong(2, newSetProducts.getProductId());
        preparedStatement.setFloat(3, newSetProducts.getProductQuantity());

        preparedStatement.executeUpdate();
    }

    public ProductsSet[] readProductSet( Connection con, long Id) throws SQLException{
        ProductsSet[] productsSets = new ProductsSet[100];
        int i = 0;
        String sqlQuery = "SELECT * FROM product_set WHERE orderId = ?";
        PreparedStatement preparedStatement = con.prepareStatement(sqlQuery);
        preparedStatement.setLong(1, Id);
        System.out.println("Looking for product..");
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()){
            ProductsSet productSet = new ProductsSet();
            // sunt aduse din baza de date rezultatele interogarii.
            long orderId = resultSet.getLong("OrderId");
            long productId =  resultSet.getLong("ProductId");
            float productQuantity = resultSet.getFloat("productQuantity");

            // popularea campurilor pentru Product
            productSet.setOrderId(orderId);
            productSet.setProductId(productId);
            productSet.setProductQuantity(productQuantity);
            productsSets[i] = productSet;
            i++;
        }
        return productsSets;
    }
    // Operatia de ReadAll:
    public ProductsSet[] readAllProductsSet(Connection connection) throws SQLException{
        ProductsSet[] productsSets = new ProductsSet[100];
        int i = 0;
        String sqlQuery  = " SELECT * FROM product_set";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        ResultSet resultSet = preparedStatement.executeQuery();  // se executa SQL query si sunt returnate rezultatele din baza de date;
        while (resultSet.next()){
            ProductsSet productsSetReadAll = new ProductsSet();
            // sunt aduse din baza de date rezultatele interogarii.
            long orderId = resultSet.getLong("OrderId");
            long productId =  resultSet.getLong("ProductId");
            float productQuantity = resultSet.getFloat("productQuantity");

            // popularea campurilor pentru Product
            productsSetReadAll.setOrderId(orderId);
            productsSetReadAll.setProductId(productId);
            productsSetReadAll.setProductQuantity(productQuantity);
            productsSets[i] = productsSetReadAll;
            i++;
        }
            return productsSets;
        }

    public void deleteProductSetByOrderId(Connection connection, long orderId) throws SQLException{
        String sqlQuery = "DELETE FROM `product_set` WHERE OrderId = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(sqlQuery);
        preparedStatement.setLong(1, orderId);
        preparedStatement.executeUpdate();
    }

    public void deleteProductSetByProductId(Connection connection, long productId) throws  SQLException{
        String sqlQuerry ="DELETE FROM `product_set` WHERE ProductId = ?";
        PreparedStatement preparedStatement  = connection.prepareStatement(sqlQuerry);
        preparedStatement.setLong(1, productId);
        preparedStatement.executeUpdate();
    }

}
