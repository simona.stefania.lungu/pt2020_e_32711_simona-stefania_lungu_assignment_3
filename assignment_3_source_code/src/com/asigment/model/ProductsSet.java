package com.asigment.model;
// Clasa creata pentru a putea avea o comanda cu mai multe produse.
public class ProductsSet {
    // Class attributes:
    private long orderId;
    private long productId;
    private float productQuantity;

    public ProductsSet() {
    }

    public ProductsSet(long orderId, long productId, float productQuantity) {
        this.orderId = orderId;
        this.productId = productId;
        this.productQuantity = productQuantity;
    }

    // Gettere:
    public long getOrderId(){
        return this.orderId;
    }
    public long getProductId(){
      return this.productId;
  }
    public float getProductQuantity() {
        return productQuantity;
    }

    // Settere:
    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public void setProductQuantity(float productQuantity) {
        this.productQuantity = productQuantity;
    }

    @Override
    public String toString() {
        return orderId+ " | "+productId+" | "+productQuantity+" | ";
    }
}
