package com.asigment.model;

public class Client {
    // Definirea atributelor clasei Client:
    private long clientId;
    private String firstName;
    private String lastName;
    private String address;
    private String phone;
    private String emailAddress;

    // Definirea unui constructor gol.
    public Client(){
    }
    // Definirea unui constructor cu parametrii corespunzatori:
    public Client(long id, String firstName, String lastName,String address, String phone, String emailAddress){
        this.clientId = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.phone = phone;
        this.emailAddress = emailAddress;
    }

    // Definirea de gettere:
    public long getClientId() {
        return clientId;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getAddress() {
        return address;
    }
    public String getPhone() {
        return phone;
    }
    public String getEmailAddress() {
        return emailAddress;
    }

    // Definirea de settere:
    public void setClientId(long clientId) {
        this.clientId = clientId;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    // Definirea metodei toString:

    @Override
    public String toString() {
        return clientId + " | " + firstName + " | " + lastName + " | " + address + " | " + phone + " | " + emailAddress;
    }
}
