package com.asigment.model;

public class Product {
    // Definirea atributelor clasei Product:
    private long productId;
    private String productName;
    private float productStock;
    private String measureUnit;
    private float productPrice;

    // Definirea unui constructor gol.
    public Product(){
    }

    // Definirea unui constructor cu parametrii corespunzatori:
    public Product( long id, String name, float productStock, String measureUnit, float price ){
        this.productId = id;
        this.productName = name;
        this.productStock = productStock;
        this.measureUnit = measureUnit;
        this.productPrice = price;
    }

    // Definirea de gettere:
    public long getProductId(){
        return this.productId;
    }
    public String getProductName(){
        return this.productName;
    }
    public float getproductStock(){
        return this.productStock;
    }
    public String getMeasureUnit(){
        return this.measureUnit;
    }
    public float getProductPrice(){
        return this.productPrice;
    }

    // Definirea de settere:
    public void setProductId(long id){
        this.productId = id;
    }
   public void setProductName(String name){
        this.productName = name;
   }
   public void setproductStock(float quantity){
        this.productStock = quantity;
   }
   public void setMeasureUnit(String um){
        this.measureUnit = um;
   }
   public void setProductPrice(float price){
        this.productPrice = price;
   }

   // Definirea metodei toString:
    @Override
    public String toString() {
        return productId+" | "+ productName+" | "+ productStock +" | "+ measureUnit+"  | "+productPrice+" | ";
//
    }
}
