package com.asigment.model;

public class Order {
    // Definirea atributelor clasei Order:
    private long orderId;
    private long clientId;
    private String orderStatus;
    private String orderDate;

    // Definirea unui constructor gol.
    public Order(){
    }

    // Definirea unui constructor cu parametrii corespunzatori:
    public Order( long orderId, long clientId, String orderStatus, String orderDate){
        this.orderId = orderId;
        this.clientId = clientId;
        this.orderStatus = orderStatus;
        this.orderDate = orderDate;
    }

    // Definirea de gettere:
    public long getOrderId() {
        return orderId;
    }

    public long getClientId() {
        return clientId;
    }
    public String getOrderStatus() {
        return orderStatus;
    }

    public String getOrderDate() {
        return orderDate;
    }

    // Definirea de settere:
    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }


    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    // Definirea metodei toString:

    @Override
    public String toString() {
        return orderId + " | " + clientId + " | " + orderStatus + " | " + orderDate;
    }
}
