package com.asigment.presentationLayer;

import com.asigment.businessLogic.ClientOperations;
import com.asigment.businessLogic.OrderOperations;
import com.asigment.businessLogic.ProductOperations;
import com.asigment.businessLogic.ProductsSetOperations;
import com.asigment.model.Client;
import com.asigment.model.Order;
import com.asigment.model.Product;
import com.asigment.model.ProductsSet;
import com.sun.org.apache.xpath.internal.operations.Or;
import com.sun.scenario.effect.impl.sw.java.JSWPhongLighting_DISTANTPeer;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLOutput;
import java.util.Scanner;

public class FileOperations {
     public File createFile() throws IOException {
         File newFile =null;
         try {
              newFile = new File("Operations.txt");
             if (newFile.createNewFile())
                 System.out.println("Fisierul a fost creat cu succes!");
         }
         catch(Exception e){
             System.out.println("Eroare de creare!");
         }
         return newFile;
     }

     public void readFile(Connection connection, File readFile) throws IOException, SQLException {
         long clientId = 0;
         long orderId = 0;
         long productId = 0;

         ClientOperations clientOperations = new ClientOperations();
         ProductOperations productOperations = new ProductOperations();
         OrderOperations orderOperations = new OrderOperations();
         ProductsSetOperations productsSetOperations = new ProductsSetOperations();
         Scanner document = new Scanner(readFile);
         while (document.hasNextLine()) {
             String data = document.nextLine();
             String[] dataSplit = data.split(":");
                 switch(dataSplit[0]) {
                     case "Insert client":
                         System.out.println("Operatia este de insert a unui client!");
                         String[] clientSplit = dataSplit[1].split(","); // Textul este divizat in alte Stringuri cand intalneste semnul virgula.
                         String[] clientDetails = clientSplit[0].split(" "); // Textul este divizat in alte Stringuri cand intalneste spatiu.
                         clientId = clientOperations.maxClientId(connection);
                         Client newClient = new Client();
                       // Se specifica cu ajutorul Setterelor noile valori ce urmeaza sa fie inserate in baza de date:
                         newClient.setFirstName(clientDetails[2]);
                         newClient.setLastName(clientDetails[1]);
                         newClient.setAddress(clientSplit[1]);
                         newClient.setClientId(++clientId);
                        // Se apeleaza metoda care efectueaza operatia de Insert:
                         clientOperations.addClient(connection, newClient);
                         break;
                     case "Delete client":
                         System.out.println("Operatia este de delete a unui client!");
                         String[] clientSplitDelete = dataSplit[1].split(",");
                         String[] clientDetalilsDelete =clientSplitDelete[0].split(" ");
                         Client newClientDelete = clientOperations.readClientbyName(connection, clientDetalilsDelete[2],clientDetalilsDelete[1]);
                         clientOperations.deleteClient(connection,newClientDelete.getClientId());
                         break;
                     case "Update client":
                         System.out.println("Operatia este de update a unui client!");
                         String[] clientSplitUpdate = dataSplit[1].split(","); // Textul este divizat in alte Stringuri cand intalneste semnul virgula.
                         String[] clientDetailsUpdate = clientSplitUpdate[0].split(" ");  // Textul este divizat in alte Stringuri cand intalneste spatiu.
                         // Pentru a putea realiza operatia de Update este necesar sa se gasesca clientul dupa firstName si lastName: ( se apeleaza metoda readClientbyName din clasa ClientOperations):
                         Client clientRead = clientOperations.readClientbyName(connection,clientDetailsUpdate[2],clientDetailsUpdate[1]); //. metoda returneaza un obiect de tip Client
                         Client newClientUpdate = new Client();
                         // Se specifica cu ajutorul Setterelor noile valori ce urmeaza sa fie updatate in baza de date:
                         newClientUpdate.setFirstName(clientDetailsUpdate[2]);
                         newClientUpdate.setLastName(clientDetailsUpdate[1]);
                         newClientUpdate.setAddress(clientSplitUpdate[1]);
                         newClientUpdate.setClientId(clientRead.getClientId());
                         // Se apeleaza metoda care efectueaza operatia de Update:
                         clientOperations.updateClient(connection,newClientUpdate);
                         break;
                    case "Report client":
                         System.out.println("Operatia este de Report Client!");
                         Client[] clients = clientOperations.readAll(connection);
                         writeClientToFile(clients);
                         break;
                     case "Insert product":
                         System.out.println("Operatia este de insert a unui product!");
                          /*Pentru a putea introduce in baza de date un nou produs este nevoie de un nou productId si astfel vom vedea cu ajutorul unui query care este ultimul
                         productId din baza de date ( in cazul in care aceasta este populata) si astfel rulam metoda din ProductOperations care face acest lucru: */
                         productId = productOperations.maxProductId(connection);
                         System.out.println("ProductId = "+productId);
                         String[] productSplitInsert = dataSplit[1].split(","); // Textul este divizat in alte Stringuri cand intalneste semnul virgula.
                         Product newProductInsert = new Product();
//                             Stringurile se transforma in float.
                         float productStock = Float.parseFloat(productSplitInsert[1]);
                         float productPrice = Float.parseFloat(productSplitInsert[2]);
                         newProductInsert.setProductName(productSplitInsert[0]);
                         newProductInsert.setproductStock(productStock);
                         newProductInsert.setProductPrice(productPrice);
                         newProductInsert.setProductId(++productId);
                         productOperations.addProduct(connection, newProductInsert);
                         break;
                     case "Update product":
//                         System.out.println("Operatia este de update a unui product!");
                            String[] productSplitUpdate = dataSplit[1].split(","); // Textul este divizat in alte Stringuri cand intalneste semnul virgula.

                         // Pentru a putea realiza operatia de Update este necesar sa se gasesca produsul  dupa productName( se apeleaza metoda readProductByName din clasa ProductOperations):
                            Product productReadbyName = productOperations.readProductByName(connection,productSplitUpdate[0]);
                            Product newProductUpdate =  new Product();
                            // Stringurile se transforma in float.
                            float productStock1 = Float.parseFloat(productSplitUpdate[1]);
                            float productPrice1 = Float.parseFloat(productSplitUpdate[2]);
                            newProductUpdate.setproductStock(productStock1);
                            newProductUpdate.setProductPrice(productPrice1);
                            newProductUpdate.setProductId(productReadbyName.getProductId());
                            newProductUpdate.setProductName(productReadbyName.getProductName());
                            productOperations.productUpdate(connection,newProductUpdate);

                         break;
                     case "Delete product":
                         System.out.println("Operatia este de delete a unui product!");
                         break;
                     case "Report product":
                         System.out.println("Operatia este de Report Product!");
                         Product[] products = productOperations.readAll(connection);
                         writeProductToFile(products);
                         break;
                     case "Order":
                         System.out.println("Operatia este de insert a unui order!");
                         orderId = orderOperations.maxOrderId(connection);
                         String[] orderSplit = dataSplit[1].split(","); // Textul este divizat in alte Stringuri cand intalneste semnul virgula.
                         String[] orderDetails = orderSplit[0].split(" "); // Textul este divizat in alte Stringuri cand intalneste spatiu.
                         String lastName = orderDetails[1];
                         String firstName = orderDetails[2];
                         String product = orderSplit[1];
                         String stock = orderSplit[2];

                         Client client = clientOperations.readClientbyName(connection, firstName, lastName);

                         Order order = new Order();
                         order.setOrderId(++orderId);
                         order.setClientId(client.getClientId());

                         orderOperations.addOrder(connection, order);
                         break;
//                     case "Delete Order":
//                         System.out.println("Operatia este de delete a unui order!");
//                         break;
//                     case "Update Order":
//                         System.out.println("Operatia este de update a unui order!");
//                         break;
                     case "Report order":
                         System.out.println("Operatia este de Report Order!");
                         Order[] orders = orderOperations.readAll(connection);
                         writeOrderToFile(orders);
                         break;
                     case "Report ProductsSet":
                         System.out.println("Operatia este de Report Products Set!");
                         ProductsSet[] productsSets = productsSetOperations.readAllProductsSet(connection);
                         writeProductsSet(productsSets);
                         break;
                     default:
                         System.out.println("Operatie gresita!");
                 }
         }
         document.close();
     }

     // Generarea unui fisier pentru Report Client:
     public void writeClientToFile(Client[] clients) throws IOException {
         int i = 0;
         File reportClient = new File("ReportClient.txt");
            if(reportClient.createNewFile()){
                System.out.println("Fisierul "+ reportClient.getName()+" a fost creat cu succes!");
            };

         FileWriter fileWriter = new FileWriter(reportClient, true);
         fileWriter.write("\n");
         fileWriter.write("________________________________________________________");
         fileWriter.write("\n");
         fileWriter.write("ClientId | FirstName | LastName | Address | Phone | email");
         fileWriter.write("\n");
         while(clients[i] != null) {
             fileWriter.write(clients[i].toString());
             fileWriter.write("\n");
             i++;
         }
         fileWriter.write("________________________________________________________");
         fileWriter.close();
     }
    // Generarea unui fisier pentru Report Order:
     public void writeOrderToFile(Order[] orders) throws IOException {
         int i = 0;
         File reportOrder = new File("ReportOrder.txt");
         if(reportOrder.createNewFile()){
             System.out.println("Fisierul "+ reportOrder.getName()+" a fost creat cu succes!");
         };

         FileWriter fileWriter = new FileWriter(reportOrder, true);

         fileWriter.write("\n");
         fileWriter.write("________________________________________________________");
         fileWriter.write("\n");
         fileWriter.write("OrderId | ClientId | Status | Date ");
         fileWriter.write("\n");
         while(orders[i] != null) {
             fileWriter.write(orders[i].toString());
             fileWriter.write("\n");
             i++;
         }
         fileWriter.write("________________________________________________________");
         fileWriter.close();

     }
    // Generarea unui fisier pentru Report Product:
    public void writeProductToFile(Product[] product) throws IOException {
         File reportProduct =  new File("ReportProduct.txt");
         if (reportProduct.createNewFile()){
             System.out.println("Fisierul "+ reportProduct.getName()+" a fost creat cu succes!");
         }
         FileWriter writeProduct = new FileWriter(reportProduct);
        writeProduct.write("\n");
        writeProduct.write("______________________________________________________________________");
        writeProduct.write("\n");
        writeProduct.write("ProductId | ProductName | ProductStock | MeasureUnit | ProductPrice ");
        writeProduct.write("\n");
        writeProduct.write("______________________________________________________________________");

        int i = 0;
        while (product[i] != null ){
            writeProduct.write("\n");
            writeProduct.write(product[i].toString());
            writeProduct.write("\n");
            i++;
            writeProduct.write("______________________________________________________________________");
        }
        writeProduct.close();
    }

    public void writeProductsSet(ProductsSet[] productsSets) throws IOException{
         File reportProductsSet =  new File("ProductsSetReport.txt");
         if (reportProductsSet.createNewFile()){
             System.out.println("Fisierul "+ reportProductsSet.getName()+" a fost creat cu succes!");
         }
         FileWriter writeProductsSet = new FileWriter(reportProductsSet);
         writeProductsSet.write("\n");
         writeProductsSet.write("_________________________________________________________________________");
         writeProductsSet.write("\n");
         writeProductsSet.write("OrderId | ProductId | ProductQuantity ");
         int i = 0;
         while (productsSets[i] != null){
             writeProductsSet.write("\n");
             writeProductsSet.write(productsSets[i].toString());
             writeProductsSet.write("\n");
             i++;
         }
         writeProductsSet.close();
    }

}

