package com.asigment.dataAccess;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {

    // creez conexiunea in mod singleton la baza de date
    static final String DB_URL = "jdbc:mysql://localhost:3306/warehouse";
    // database credentials
    static final String USER = "root";
    static final String PASS = "God19Whelp%Simo!ed";

    public Connection connection;
    public static DBConnection db;

    private DBConnection() {
        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            if(connection != null) {
                System.out.println("Connecting to database...");
            }
        } catch (Exception e) {
            // handle errors for Class.forName
            e.printStackTrace();
        }
    }
    public static synchronized DBConnection getConnection() {
        // pentru a ne asigura ca db este instantiat o singura data, indiferent de unde se apeleaza
        if (db == null) {
            db = new DBConnection();
        }
        return db;
    }
}
